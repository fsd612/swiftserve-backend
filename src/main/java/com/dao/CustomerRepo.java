package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import org.springframework.stereotype.Repository;
import com.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

	Customer findByEmail(String email);

//	 @Modifying
//	    void updatePasswordByEmail(String email);
//	
//	@Modifying
//	@Query("UPDATE Customer c SET c.password='' WHERE c.email=:email")
//
//	void updatePasswordByEmail(@Param("email")String email);

	

}
