package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.CartItems;

public interface CartItemsRepo extends JpaRepository<CartItems, Integer> {

    List<CartItems> findByCustomer_CustId(int custId);

}