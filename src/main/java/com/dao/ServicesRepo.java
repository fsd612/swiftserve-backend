package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Services;

@Repository
public interface ServicesRepo extends JpaRepository<Services, Integer> {
    List<Services> findByCompanyId(int id);

	void findByEmail(String email);

	
}