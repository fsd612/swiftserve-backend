package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.CartItems;

@Service
public class CartItemsDao {

    @Autowired
    private CartItemsRepo cartItemsRepo;

    public List<CartItems> findCartBycustId(int custId) {
        return cartItemsRepo.findByCustomer_CustId(custId);
    }

    public String regCartbyCustId(CartItems cartItems, int custId) {
      
        cartItems.getCustomer().setCustId(custId);
        cartItemsRepo.save(cartItems);
        return "CartItems Added Successfully";
    }
    
    public String delCartItemsbyid(int cartId){
    	cartItemsRepo.deleteById(cartId);
    	return "Removed SuccessFully";
    	
    	
    }
}
