package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Admin;

@Service
public class AdminDao {
	
	
	@Autowired
	AdminRepo adminRepo;
	
	PasswordEncoder passwordEncoder;
	
	public AdminDao(AdminRepo adminRepo){
		this.passwordEncoder=new BCryptPasswordEncoder();
	}
	
	public String getRegAdmin(Admin admin) {
		System.out.println(admin.getPassword());
		admin.setPassword(this.passwordEncoder.encode(admin.getPassword()));
		
		adminRepo.save(admin);
		return "Registered Sucesfully";
	}

	public List<Admin> getAllAdmin() {
		return adminRepo.findAll();
	}
	
	public boolean validateAdmin(String email,String password){
		Admin admin =adminRepo.findByEmail(email);
		if(admin==null){
			return false;
		}
		return passwordEncoder.matches(password, admin.getPassword());
	}
	
	
	
}
