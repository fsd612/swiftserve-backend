package com.dao;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.model.Company;
import com.model.MyServices;

@Service
public class CompanyDao{

    @Autowired
    CompanyRepo companyRepo;
    
    @Autowired
    private JavaMailSender javaMailSender;
    
    PasswordEncoder passwordEncoder;
    
    public CompanyDao (CompanyRepo companyRepo){
 
    	this.passwordEncoder=new BCryptPasswordEncoder();
    }

    public String getRegCompany(Company company) {
    	 company.setPassword(this.passwordEncoder.encode(company.getPassword()));
          companyRepo.save(company);
        return "Company Registered Successfully";
    }

    public List<Company> getRegCompany() {
        return companyRepo.findAll();
    }
    
    public String delCompanyById(int id){
    	 companyRepo.deleteById(id);
    	 return "Deleted Successfully";
    }
    

    
    
    public MyServices validate(String email,String password){
    	Company company = companyRepo.findByEmail(email);
    
    	if(company==null){
    		return new MyServices(0,false,"");
    	}
    	boolean status=passwordEncoder.matches(password, company.getPassword());
    	
    	return new MyServices(company.getId(),status,company.getName());
    }
    
    public Company getCompanyByEmail(String email){
    	return companyRepo.findByEmail(email);
    }
    
    public boolean validateCompanyStatus(String email){
    	Company company=companyRepo.findByEmail(email);
    	String status = company.getStatus();
    	
    	if(company==null||status.equals("inactive")){
    	
    		return false;
    	}
    	return true;
    		
    		
    }
    
    
    
    public Company updateCompany(Company company){
		int id = company.getId();
		if(companyRepo.findById(id).orElse(null) != null){
			companyRepo.save(company);
			return company;
		}
		return new Company();
    }
    
    @Test
    @Transactional
    public String updateCompanyStatusById(int id){
        companyRepo.updateStatusById(id);
        Company company =companyRepo.findById(id).orElse(null);
        if(company !=null && company.getStatus().equals("active")){
        	sendActivationEmail(company.getEmail(),company.getName());
        }
        return "Updated Status Successfully";
    }

	public Company getCompanyById(int id) {
	return companyRepo.findById(id).orElse(null);
	}
	
	public Company getCompByCompd(int id)
	{
		return companyRepo.findById(id).orElse(null);
	}
	
	private void sendActivationEmail(String email,String name){
		MimeMessage mimeMessage=javaMailSender.createMimeMessage();
		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
		try{
			mimeMessageHelper.setTo(email);
			mimeMessageHelper.setSubject("Account Activation");
			mimeMessageHelper.setText("Dear "+name+" Your Account is Activated and Now You can Add Your services,Thank You for Registering With us ");
			javaMailSender.send(mimeMessage);
		}catch(MessagingException e){
			e.printStackTrace();
		}
	}

	
}
