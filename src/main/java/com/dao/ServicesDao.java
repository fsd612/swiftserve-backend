package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Company;
import com.model.Services;

@Service
public class ServicesDao {
	
	
	@Autowired
	ServicesRepo serviceRepo;
	
	public String getServices(Services service)
	{
		 serviceRepo.save(service);
		 return "services registered successfully";
	}
	
	public List<Services> getAllServices()
	{
		return serviceRepo.findAll();
	}
	
	public Services getServicesByCompd(int serId)
	{
		return serviceRepo.findById(serId).orElse(null);
	}
	
	public List<Services> getServicesByCompanyId(int id) {
	    return serviceRepo.findByCompanyId(id);
	}
	

	
	public String regServicesByCompid(Services services, int id)
	{
	    // Retrieve the company entity by ID
	    Company company = new Company();
	    company.setId(id);
	    services.setCompany(company);

	    // Save the service
	    serviceRepo.save(services);

	    return "services registered successfully";
	}
	
	
	
	public Services updateServices(Services services){
		int servId=services.getServicesId();
		Services existServices =serviceRepo.findById(servId).orElse(null);
		
		if(existServices!=null){
			serviceRepo.save(services);
			return services;
			
		}
				return new Services();
	}
		
	public String delServicesById(int servicesId){
		serviceRepo.deleteById(servicesId);
		return "Deleted Succesfully";
	}
	
	

	
}