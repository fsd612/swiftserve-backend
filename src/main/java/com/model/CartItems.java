package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class CartItems {
	
	@Id@GeneratedValue
    private int cartId;
    private String cName;
    private String image;
    private String descr;
    private String serName;
    private String number;
    private String techName; 
    private String price;
	
    
    @ManyToOne
    @JoinColumn(name="custId")
    private Customer customer;


	public CartItems(int cartId, String cName, String image, String descr, String serName, String number,
			String techName, String price, Customer customer) {
		super();
		this.cartId = cartId;
		this.cName = cName;
		this.image = image;
		this.descr = descr;
		this.serName = serName;
		this.number = number;
		this.techName = techName;
		this.price = price;
		this.customer = customer;
	}


	public CartItems() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getCartId() {
		return cartId;
	}


	public void setCartId(int cartId) {
		this.cartId = cartId;
	}


	public String getcName() {
		return cName;
	}


	public void setcName(String cName) {
		this.cName = cName;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}


	public String getSerName() {
		return serName;
	}


	public void setSerName(String serName) {
		this.serName = serName;
	}


	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getTechName() {
		return techName;
	}


	public void setTechName(String techName) {
		this.techName = techName;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}



    
}
