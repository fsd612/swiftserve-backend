package com.model;





import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Customer {

	@Id@GeneratedValue
	private int custId;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	
	
	@OneToMany(mappedBy="customer",cascade = CascadeType.ALL)
	List<Booking> booking = new ArrayList<Booking>();
	
	@OneToMany(mappedBy="customer",cascade = CascadeType.ALL)
	List<CartItems> cartItems=new ArrayList<CartItems>();

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public Customer() {
		super();
		
	}

	public Customer(int custId, String firstName, String lastName, String email, String password) {
		
		super();
		this.custId = custId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		
	}
	
	
	

	
	
	
}
