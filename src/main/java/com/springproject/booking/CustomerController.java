package com.springproject.booking;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.dao.CustomerDao;
import com.model.Customer;
import com.model.MyCustomer;
import com.model.MyServices;


@RestController
@CrossOrigin
public class CustomerController {

    @Autowired
    CustomerDao customerDAO;
    
    @PostMapping("/getRegCustomer")
    @CrossOrigin
    public String getRegCustomer(@RequestBody Customer customer){
        return customerDAO.getRegCustomer(customer);
    }
    
    @GetMapping("/getAllCustomer")
    @CrossOrigin
    public List<Customer> getAllCustomer(){
        return customerDAO.getAllCustomer();
    }
    
    @GetMapping("/validateCustomer/{email}/{password}")
    @CrossOrigin
    public MyServices validateCustomer(@PathVariable String email, @PathVariable String password) {
        return customerDAO.validateCustomer(email, password);
    }
    
    
    @GetMapping("/findByEmail/{email}")
    @CrossOrigin
    public Customer findByEmail(@PathVariable String email){
    	return customerDAO.findByEmail(email);
    }

    
    @GetMapping("sendOtpEmail/{email}")
    @CrossOrigin
    public String sendOtpEmail(@PathVariable String email){
    	return customerDAO.sendOtpEmail(email);
    }
    
    
    @PutMapping("/updateCustomerPassword")
    @CrossOrigin
    public Customer updateCustomerPassword(@RequestBody Customer customer){
    	return customerDAO.updateCustomerPassword(customer);
    }

    
    
//    @PutMapping("updateCustomerPassword")
//   
//    public Customer updateCustomerPassword(@RequestBody Customer customer){
//    	return customerDAO.updateCustomerPassword(customer);
//    }
    
   
}
