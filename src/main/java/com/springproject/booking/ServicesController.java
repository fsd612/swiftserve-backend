package com.springproject.booking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ServicesDao;
import com.model.Services;

@RestController
@CrossOrigin
public class ServicesController {
	@Autowired
	ServicesDao servicesDAO;

	@PostMapping("/postServices")
	@CrossOrigin
	public String regServices(@RequestBody Services services)
	{
		 servicesDAO.getServices(services);
		 return "Services Registered Successfully";
	}

	@GetMapping("/displayServices")
	@CrossOrigin
	public List<Services> displayServices()
	{
		return servicesDAO.getAllServices();
	}

	@GetMapping("/getServicesById/{serId}")
	@CrossOrigin
	public Services getServiceById(@PathVariable int serId) {
	    Services ser = servicesDAO.getServicesByCompd(serId);
	    if (ser != null) {
	        return ser;
	    }
	    return new Services();
	}
	
	@GetMapping("/getServicesByCompanyId/{id}")
	@CrossOrigin
	public List<Services> getServicesByCompanyId(@PathVariable int id) {
	    return servicesDAO.getServicesByCompanyId(id);
	}

	@PostMapping("/regServicesByCompId/{id}")
	@CrossOrigin
	public String regServicesById(@RequestBody Services services,@PathVariable int id)
	{
		servicesDAO.regServicesByCompid(services, id);
		return "Saved successfully";
	}
	
	
	@PutMapping("/updateServices")
	@CrossOrigin
	public Services updateServices(@RequestBody Services services){
		return servicesDAO.updateServices(services);
		
	}
	
	@DeleteMapping("delServicesById/{servicesId}")
	@CrossOrigin
	public String delServicesById(@PathVariable int servicesId){
		return servicesDAO.delServicesById(servicesId);
	}
	
	
}