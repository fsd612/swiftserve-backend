package com.springproject.booking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BookingDao;
import com.dao.CustomerRepo;
import com.model.Booking;
import com.model.Customer;
import com.model.Services;
import com.utility.EmailUtility;

@RestController
public class BookingController {

	
	@Autowired
	BookingDao bookingDAO;
	
	@Autowired
	private CustomerRepo customerRepo;
	
	@Autowired
	EmailUtility emailUtility;
	
	
	@PostMapping("/getRegBooking")
	@CrossOrigin
	public String getRegBooking(@RequestBody Booking booking){
		if(booking.getCustomer()==null){
			return "customer Is requiered";
			
		}
		
		Customer customer = customerRepo.findById(booking.getCustomer().getCustId()).orElse(null);
		if(customer == null){
			return "customer Not found";
		}
		booking.setCustomer(customer);
		return bookingDAO.getRegBooking(booking);
	}
	
	@GetMapping("/getAllBookings")
	@CrossOrigin
	public List<Booking>getAllBookings(){
		return bookingDAO.getAllBookings();
	}
	
	
	@GetMapping("/findBookingByCustId/{custId}")
	@CrossOrigin
	public List<Booking> findBookingByCustId(@PathVariable int custId){
		return bookingDAO.findBookingByCustId(custId);
	}
	
	@PostMapping("/regBookbyCustId/{custId}")
	@CrossOrigin
	public String regBookbyCustId(@RequestBody Booking booking ,@PathVariable int custId){
		bookingDAO.regBookbyCustId(booking, custId);
		return "Stored SuccesfullY";
	}
	
	
//	@PostMapping("/getRegisterBooking")
//	@CrossOrigin
//	public String getRegisterBooking(@RequestBody Booking booking){
//		if(booking.getCustomer()==null){
//			return "customer is Required";
//		}
//		Customer customer = customerRepo.findById(booking.getCustomer().getCustId()).orElse(null);
//		if(customer ==null){
//			return "customer not found";
//		}
//		
//		booking.setCustomer(customer);
//		String result = bookingDAO.getRegBooking(booking);
//		
//		Services service = booking.getServiceName();
//		String serviceEmail=service.getEmail();
//		if(serviceEmail!=null && !serviceEmail.isEmpty()){
//			 String subject = "New Booking Registered";
//	            String message = "A new booking has been registered for your service.";
//	            try{
//	            	emailUtility.sendOtpEmail(serviceEmail, service.getCompanyName(), message);
//	            }catch(NotFoundException e){
//	            	e.printStackTrace();
//	            }
//		}
//		return result;
//		
//	}
	
	
}
