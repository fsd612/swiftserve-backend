package com.springproject.booking;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CompanyDao;
import com.dao.ServicesRepo;
import com.model.Company;
import com.model.MyServices;

@RestController

public class CompanyController {

	@Autowired
	CompanyDao companyDAO;
	
	@Autowired
    ServicesRepo serviceRepo;
	
	@PostMapping("/getRegcompany")
	@CrossOrigin
	public String getRegCompany(@RequestBody Company company)
	{
		return companyDAO.getRegCompany(company);
    }
	
	@GetMapping("/getAllCompany")
	@CrossOrigin
	public List<Company> getAllCompany(){
		return companyDAO.getRegCompany();
	}
	
	
	

	
	@GetMapping("/validate/{email}/{password}")
	@CrossOrigin
	public MyServices validate(@PathVariable String email,@PathVariable String password){
		
		return companyDAO.validate(email, password);
	}
	
	@GetMapping("/validateCompanyStatus/{email}")
	@CrossOrigin
	public boolean validateCompanyStatus(@PathVariable String email){
		return companyDAO.validateCompanyStatus(email);
	}
	
	
	
	@DeleteMapping("delCompanyById/{id}")
	@CrossOrigin
	public String delCompanyById(@PathVariable int id){
		 return  companyDAO.delCompanyById(id);
		
	}
	
	@PutMapping("/updateById/{id}")
	@CrossOrigin
	public String updateCompanyById(@PathVariable int id){
		return companyDAO.updateCompanyStatusById(id);
	}
	
	
	
	@GetMapping("/getCompanyById/{id}")
	public Company getCompnayById(@PathVariable int id){
		Company company= companyDAO.getCompanyById(id);
		if(company!=null){
			return company;
		}
		return new Company();
	}
	
	@PutMapping("/updateCompany")
	@CrossOrigin
	public Company updateCompany(@RequestBody Company company){
		return companyDAO.updateCompany(company);
	}
	
	@GetMapping("/getCompById/{servicesId}")
	public Company getServiceById(@PathVariable int servicesId) {
	    Company com = companyDAO.getCompByCompd(servicesId);
	    if (com != null) {
	        return com;
	    }
	    return new Company();
	}
}
