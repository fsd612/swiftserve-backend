package com.springproject.booking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AdminDao;

import com.model.Admin;

@RestController
@CrossOrigin
public class AdminController {
	
	@Autowired
	AdminDao adminDao;
	
	@PostMapping("/getRegAdmin")
	public String getRegAdmin(@RequestBody Admin admin){
		return adminDao.getRegAdmin(admin);
	}
	
	@GetMapping("/getAdmin")
	public List<Admin> getAllAdmin(){
		return adminDao.getAllAdmin();
	}
	
	
	@GetMapping("/validateAdmin/{email}/{password}")
	@CrossOrigin
	public boolean validateAdmin(@PathVariable String email,@PathVariable String password){
		return adminDao.validateAdmin(email, password);
	}

}
